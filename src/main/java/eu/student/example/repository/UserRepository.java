package eu.student.example.repository;

import java.util.List;

import eu.student.example.domain.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
	
	 /**
     * Find persons like first name.
     */
    public List<User> findByAddressLike(String address);
    
    
}
