package eu.student.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.student.example.domain.Student;

public interface StudentRepository extends JpaRepository<Student, String> {

	
	
}
