package eu.student.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Student {
	
	
		@Id
	    @NotNull
	    @Size(max = 64)
	    @Column(name = "firstName", nullable = false, updatable = true)
	    private String firstName;
	
	    @NotNull
	    @Size(max = 64)
	    @Column(name = "lastName", nullable = false, updatable = true)
	    private String lastName;
	    
	    Student(){
	    	
	    }
	    
	    public Student(final String firstName, final String lastName) {
	        this.firstName = firstName;
	        this.lastName = lastName;
	    }

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
	    
		@Override
		public String toString(){
			
			return this.firstName+this.lastName;
		}
	    
}
