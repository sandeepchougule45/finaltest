package eu.student.example.controller;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.student.example.domain.Student;
import eu.student.example.service.StudentService;

@RestController
public class StudentController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
	private final StudentService studentService;
	
	//@Autowired
	@Inject
	public StudentController(final StudentService studentService){
		this.studentService = studentService;
	}
	
	
	@RequestMapping(value ="/studentCreate", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	public Student createStudent(@RequestBody @Valid final Student student)
	{
		  LOGGER.debug("Received request to create the student {}", student);
		  LOGGER.warn("Received request to create the studentpppp {}", student);
		return studentService.save(student);
	}
	
	@RequestMapping(value="/studentList", method= RequestMethod.GET)
	public List<Student> getStudentsList(){
		 LOGGER.debug("Received request to fetch all the students ");
		 List<Student> studentsList = studentService.studentList();
	        Iterator<Student> it = studentsList.iterator();
	        Student student = null;
	        while(it.hasNext()){
	        	 student = it.next();
	        	  LOGGER.debug("Student name-->"+student.getFirstName()+"LastNAme"+student.getLastName());
	        }
		return studentsList;
	} 
	

}
