package eu.student.example.controller;

import eu.student.example.domain.User;
import eu.student.example.service.UserService;
import eu.student.example.service.exception.UserAlreadyExistsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

import java.util.List;



/*
 * Description : 
 * It's annotated with @RestController. The difference between this and @Controller annotation is the former also implies @ResponseBody on every method,
 *  which means there is less to write
 *  since from a RESTful web service we are returning JSON objects anyway.
 *  @RequestMapping maps the createUser() to the POST request on the /user url.
 *  Method takes the User object as a parameter. It is created from the body of the request thanks to @RequestBody annotation.
 *  It is then validated, which is enforced by @Valid.
 *  The UserService will be injected to the constructor, and User object is passed to its save() method for storage.
 *  After storing, the stored User object will be returned. Spring will convert it back to JSON automatically,
 *  even without @ResponseBody annotation which is default for @RestController.
 * 
 * 
 */
@RestController
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @Inject
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public User createUser(@RequestBody @Valid final User user) {
        LOGGER.debug("Received request to create the {}", user);
        return userService.save(user);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public User updateUser(@RequestBody @Valid final User user) {
        LOGGER.debug("Received request to update the {}", user);
        return userService.update(user);
    }
    
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> listUsers() {
        LOGGER.debug("Received request to list all users");
        
        return userService.getList();
    }

    
    @RequestMapping(value = "/searchByAddress", params= "address", method = RequestMethod.GET)
    public List<User> listByAddress(@RequestBody final String address) {
        LOGGER.debug("Received request to list all users");
        
        return userService.getAddressList(address);
    }
    
    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleUserAlreadyExistsException(UserAlreadyExistsException e) {
        return e.getMessage();
    }

}
