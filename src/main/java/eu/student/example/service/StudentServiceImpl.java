package eu.student.example.service;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import eu.student.example.domain.Student;
import eu.student.example.repository.StudentRepository;

@Service
@Validated
public class StudentServiceImpl implements StudentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentService.class);
	private final StudentRepository repository;
	
	@Inject
	public StudentServiceImpl(final StudentRepository studentRepository) {
		this.repository = studentRepository;
	}
	
	
	@Override
	public Student save(Student student) {
		// TODO Auto-generated method stub
		return repository.save(student);
		
	}

	@Override
	@Transactional(readOnly = true)
	public List<Student> studentList() {
		// TODO Auto-generated method stub
		
		return repository.findAll();
	}
	
	
	

}
