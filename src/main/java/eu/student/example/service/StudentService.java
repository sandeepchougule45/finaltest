package eu.student.example.service;

import java.util.List;

import eu.student.example.domain.Student;

public interface StudentService {

	Student save(Student student);
	List<Student> studentList();
}
