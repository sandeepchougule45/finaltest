package eu.student.example.service;

import eu.student.example.domain.User;

import java.util.List;

/*
 * @Description :
 * For the test to pass we need only an interface for UserService, because it is not even created, but merely mocked. 
 * It should take User objects into the save() methods 
 * which will be used to save them, then it should return saved User object back to the caller.
 * 
 */

public interface UserService {

    User save(User user);
    User update(User user);
    List<User> getList();
    List<User> getAddressList(String address);
    
}
