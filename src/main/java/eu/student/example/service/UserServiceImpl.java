package eu.student.example.service;

import eu.student.example.domain.User;	
import eu.student.example.repository.UserRepository;
import eu.student.example.service.exception.UserAlreadyExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Validated
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository repository;

    @Inject
    public UserServiceImpl(final UserRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public User save(@NotNull @Valid final User user) {
        LOGGER.debug("Creating {}", user);
        User existing = repository.findOne(user.getId());
        if (existing != null) {
            throw new UserAlreadyExistsException(
                    String.format("There already exists a user with id=%s", user.getId()));
        }
        return repository.save(user);
    }

    @Override
    @Transactional
    public User update(@NotNull @Valid final User user) {
        LOGGER.debug("Updating {}", user);
        User existing = repository.findOne(user.getId());
        if (existing != null) {
          /*  throw new UserAlreadyExistsException(
                    String.format("There already exists a user with id=%s", user.getId()));*/
            return repository.save(user);
        }
        return repository.save(user);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<User> getList() {
        LOGGER.debug("Retrieving the list of all users");
        return repository.findAll();        
    }

  
    
    
    @Override
    @Transactional(readOnly = true)
    public List<User> getAddressList(String address) {
        LOGGER.debug("getAddressList {}", address);
        
        if(address!=null)
        	address = "address";
        return repository.findByAddressLike(address);    
        
    }
}
