package eu.student.example.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.student.example.domain.Student;
import eu.student.example.domain.User;
import eu.student.example.service.StudentService;
import eu.student.example.util.StudentUtil;
import eu.student.example.util.UserUtil;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {

	@Mock
	private StudentService studentService;

	private StudentController studentController;

	@Before
	public void setUp() throws Exception{

		studentController = new StudentController(studentService);
	}

	@Test
	public void shouldCreateStudent() throws Exception{
		final Student savedStudent = stubServiceToReturnStoredStudent();
		final Student student = StudentUtil.createStudent();
		Student returnedStudent = studentController.createStudent(student);
		// verify user was passed to UserService
		verify(studentService, times(1)).save(student);
		assertEquals("Returned user should come from the service", savedStudent, returnedStudent);

	}
	private Student stubServiceToReturnStoredStudent() {
		final Student student = StudentUtil.createStudent();
		when(studentService.save(any(Student.class))).thenReturn(student);
		return student;
	}
	
	@Test
	public void shouldListAllStudents() throws Exception{
		stubServiceToReturnExistingStudentList(10);
		Collection<Student> students= studentController.getStudentsList();
		assertNotNull(students);
		assertEquals(10,students.size());
		verify(studentService, times(1)).studentList();
	}
	
	private void stubServiceToReturnExistingStudentList(int howMany) {
		when(studentService.studentList()).thenReturn(StudentUtil.createStudentList(howMany));
		
	}
}
