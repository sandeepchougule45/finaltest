package eu.student.example.util;

import java.util.ArrayList;
import java.util.List;

import eu.student.example.domain.Student;

public class StudentUtil {
	
	private static final String FIRSTNAME ="firstName";
	private static final String LASTNAME = "lastName";
	
	private StudentUtil(){
		
	}
	
	public static Student createStudent(){
		return new Student(FIRSTNAME,LASTNAME);
	}
	
	public static List<Student> createStudentList(int howMany){
		List<Student> studentList = new ArrayList<>();
		for(int i=0;i<howMany;i++){
			studentList.add(new Student(FIRSTNAME+"#"+i, LASTNAME));
			
		}
		
		return studentList;
		
	}
	
}
