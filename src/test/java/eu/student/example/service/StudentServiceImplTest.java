package eu.student.example.service;

import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.student.example.domain.Student;
import eu.student.example.repository.StudentRepository;
import eu.student.example.util.StudentUtil;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceImplTest {
	@Mock
	private  StudentRepository studentRepository;
	
	private StudentService studentService;
	
	@Before
	public void setUp() throws Exception{
		studentService = new StudentServiceImpl(studentRepository);
	}
	@Test
	public void shouldSaveNewStudent_GivenThereDoesNotExistOneWithSameName_ThentheSavedStudentShouldBeReturned() throws Exception{
		final Student savedStudent = stubRespositorytoReturnStudentOnSave();
		final Student student = StudentUtil.createStudent();
		final Student returnedStudent = studentService.save(student);
		
		verify(studentRepository,times(1)).save(student);
		 assertEquals("Returned Student should come from repository",savedStudent,returnedStudent);
		
	}
	
	private Student stubRespositorytoReturnStudentOnSave(){
		Student student = StudentUtil.createStudent();
		when(studentRepository.save(any(Student.class))).thenReturn(student);
		return student;
	}

}
